#
#  IndController.rb
#  ind
#
#  Created by Thomas Klemz on 3/10/12.
#  Copyright 2012 Black Koala Software, LLC. All rights reserved.
#

class IndController < NSWindowController
    
    attr_accessor :destination_path, :old_name_text, :new_name_text, :go, :progress
    
    GEN_ERR = [
            "\ncan't go yet...\n", 
            "\nyou're doing it all wrong...\n",
            "\nred means STOP. then THINK. then GO. in that order.\n",
            "\ninconceivable!\n", 
            "\ndo you want me to send you back to where you were? unemployed, in greenland?!\n"
    ]
    PATH_ERR = "select the inx directory"
    OLD_NAME_ERR = "enter the text to be replaced"
    NEW_NAME_ERR = "enter the replacement text"
    
    def awakeFromNib
        old_name_text.resignFirstResponder
        new_name_text.resignFirstResponder
        destination_path.window.makeFirstResponder destination_path
    end
    
    def browse(sender)
        dialog = NSOpenPanel.openPanel
        dialog.canChooseFiles = false
        dialog.canChooseDirectories = true
        dialog.allowsMultipleSelection = false
        
        if dialog.runModalForDirectory(nil, file:nil) == NSOKButton
            destination_path.stringValue = dialog.filenames.first
            # make replace text active
            old_name_text.window.makeFirstResponder old_name_text
        elsif !destination_path.stringValue.empty?
            # user canceled, but there is text so make replace text active
            old_name_text.window.makeFirstResponder old_name_text
        else
            # back to ground zero
            destination_path.window.makeFirstResponder destination_path
        end
    end
    
    def convert(sender)
        ind ||= Ind.new
        @count ||= 0
        path = destination_path.stringValue
        old_name = old_name_text.stringValue
        new_name = new_name_text.stringValue
        
        unless path.empty? || old_name.empty? || new_name.empty?
            @count = 0
            Logger.log "\ngoing...\n"
            progress.startAnimation nil
            ind.go(path, old_name, new_name)
            progress.stopAnimation nil
        else
            Logger.log GEN_ERR[@count], :red
            Logger.log PATH_ERR if path.empty?
            Logger.log OLD_NAME_ERR if old_name.empty?
            Logger.log NEW_NAME_ERR if new_name.empty?
            
            @count = (@count + 1) % (GEN_ERR.length)
        end
    end
end
