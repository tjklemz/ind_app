#
#  Logger.rb
#  ind
#
#  Created by Thomas Klemz on 3/12/12.
#  Copyright 2012 Black Koala Software, LLC. All rights reserved.
#

# uses a ruby-esque Singleton pattern (aka not enforced)

class Logger
    attr_accessor :console
    
    def awakeFromNib
        @@console = console
        console.setFont NSFont.fontWithName("Menlo", size:14)
    end

    def self.log(msg, color=nil)
        colors = {:red => NSColor.colorWithCalibratedRed(0.8, green: 0, blue: 0, alpha: 1), 
                :black => NSColor.blackColor, 
                :green => NSColor.colorWithCalibratedRed(0, green: 0.7, blue: 0, alpha: 1) }
    
        if @@console
            msg += "\n"
            
            if(@@console.string.length > 0)
                theEnd = NSMakeRange(@@console.string.length - 1, 1)
                theEnd.location += msg.length
                @@console.textStorage.mutableString.appendString msg
                @@console.scrollRangeToVisible(theEnd)
            else
                @@console.string += msg
            end

            text_color = colors[color]
            if !text_color.nil?
                @@console.setTextColor(text_color, range: NSMakeRange(@@console.string.length-msg.length, msg.length-1))
            end
        else
            puts msg
        end
    end
end
