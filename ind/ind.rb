# Copyright 2012 Black Koala Software, LLC
# You are free to modify, use, and redistribute this code without restriction. (FSF)

class Ind
    
    def go(path, old_name, new_name)
        @path = self.fix_path(path)
        # replace forward slashes with colons, don't need to escape spaces
        @new_name_colons = new_name.gsub(/\//, ':')
        @old_name_colons = old_name.gsub(/\//, ':')
        # escape spaces for slashes
        @new_name_slashes = new_name.gsub(/\ /, '%20')
        @old_name_slashes = old_name.gsub(/\ /, '%20')
        
        Logger.log "About to change links from '#{old_name}' to '#{new_name}'"
		Logger.log "\nGoing to loop through directory '#{@path}'\n"
        
        count = 0
        
        Dir.glob(@path + '*.inx') do |inx|
			Logger.log "in dir '#{@path}', parsing file '#{inx}'"
			
			new_content = self.fix_links(inx)
			self.write_new inx, new_content
            count += 1
		end
        
        if count == 0
            Logger.log "\nOkay.\n", :green
            Logger.log "There weren't any .inx files in dir '#{@path}'.\nBut everything went okay.\n"
        else
            Logger.log "\nSuccess!", :green
            Logger.log "You fixed #{count} file#{'s' unless count == 0}.\n"
        end
    end
    
protected
    
    def fix_path(path)
        path = path.gsub(/^[^:]*:/, '/').gsub(/:/,'/')
        path = File.expand_path(path)
        path += '/' unless path[-1] == '/'
    end
    
    def fix_links(file)
        File.open(file, 'r') do |f|
            fixed_slashes = f.read.gsub(/\/#{@old_name_slashes}\//, "/#{@new_name_slashes}/")
            fixed_slashes.gsub(/:#{@old_name_colons}:/, ":#{@new_name_colons}:")
        end
    end
    
    def write_new(file, new_content)
        File.open(file, 'w+') do |f| 
            f.puts new_content
        end
    end
end
